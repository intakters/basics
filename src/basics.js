// comments - a "//" karakterek utan irt szoveget a programunk figyelmen kivul hagyja a sor vegeig

/* mutliple line comments -
  a
  "/*"
  karakterek
  utan
  minden
  komment lesz amig nincs
  a tukorkepuk
*/


// data types - number, string, boolean, function, object

// valtozok / variables - ertekek amiket definialhatunk es kesobb felhasznalhatunk, modosithatunk
// syntax - "var" keyword utan egy szabadon valasztott név
//          majd "=" es a valtozo erteke (itt mar hasznalhatunk korabban definialt valtozokat is)

// a nevnek betuvel kell kezdodnie, es nem tartalmazhat ékezetes betűket vagy specialis karaktereket (./=-...) kivetel "_" (underscore character) amivel elvalaszthatunk tobb szobol allo neveket
//                                       masik szokas hogy minden uj szot nagy betuvel irsz

// number
var egesz_szam = 21
var tort_szam = 1.03004
var negativ_szam = -40
var elozo_hatvanya = negativ_szam * negativ_szam

// string - (meaning a string of characters)
// ket " vagy ' koze irt szoveg, amiben lehet mindenfele karakter (majdnem...)
var szoveg = "nem tudom miért"
var masik_szoveg = 'azt senki sem tudja'

// boolean - (named after George Boole)
// "igazsag tartalom", csak 2 fajta erteke lehet: "true" vagy "false"
// kesobb ezek segitsegevel tudunk kulonbozo dolgokat csinalni attol fuggoen hogy valami igaz-e vagy sem
var ez_hasznos_ehet = true
var ami_nem_hamis_az = true
var teljes_kaosz = false

// function - fuggveny, method, procedura, algoritmus stb
// a programunk legfontosabb alkotoeleme
// egy block of code ami rendelkezik 1 vagy tobb inputtal es 1 outputtal
// a functionon belul definialhatunk valtozokat amikkel elvegzunk valamilyen szamitast
// aztan a "return" keyworddel visszaad(hat)unk valamit a function meghivojanak
// syntax - "function" keyword, majd zarojelek kozt az input(ok) amiket várunk, ezeken a neveken tudjuk majd oket hasznalni a fuggvenyunkon belul, aminek hatarait a "{}" jelek jelzik

var ketsegbeejto = function(input_szoveg){ // <- elkezdodik a function body

  // ez itt mar a functionon beluli kod amig be nem zarjuk a "}" zarojellel
  // itt hasonloan csinalhatunk uj valtozokat, amikre szuksegunk van
  var ketseg_kelto_befejezes = "... vagy nem."
  // return keyword utan leirjuk mit szeretnek visszadni outputkent
  return input_szoveg + ketseg_kelto_befejezes
  // ez a function hozzaadja a "ketseg_kelto_befejezes"-t ahhoz amit inputkent megkap

} // <- bezarodott a function body

// a fuggvenyek meghivasa / function calling
// a function neve utan zarojelek, koztuk az input amivel meg szeretnenk hivni
// alabb meghivom a ketsegbeejto-t egy uj stringgel es amit visszaad azt beleteszem egy valtozoba
var amit_visszakapok = ketsegbeejto( "ez egész érthető" )

// object - az eddig felsorolt data type-ok osszekomponalasara szolgalo tipus aminek segitsegevel komplexebb data typeokat hozhatunk letre
// syntax - ket "{}" kozott,
//           vesszokkel elvalasztott nevek (property of the object) es ertekek,
//           amik kozt ":" van (think CSS csak a ";" helyett sima vesszo ",")
// egy property erteke lehet barmelyik data type, akar object is

var zold_haromszog = {
  sarkok : 3, // egy number property
  szin : "#0f0", // egy string property
  oldalai_parhuzamosak_egymassal : false // egy bool property
}

// kesobb egy object property-jere pont "." syntaxxal tudunk hivatkozni
// pl
var az_elobbi_szine = zold_haromszog.szin

// tovabbi peldak

var ket_dimenzios_vektor = {
  x : 10,
  y : 120
}

var ficko = {
  nev : "Terry",
  hangulat : "terrible",
  szuletett : 1921,
  kedvenc_forma : zold_haromszog,
  kor : function(jelenlegi_ev){
    //                    this keyword azt az objectet jelenti amin a function van.
    return jelenlegi_ev - this.szuletett
    // ez a function visszadja hany eves Terry attol fuggoen hogy milyen evet irunk
  }
}

var terry_kora = ficko.kor( 2018 )


// array - lista, tomb stb, egy specialis object aminek a property-ei szamok
// syntax - "[]" kozott vesszovel elvalasztott ertekek
// a listak elemeinek szamozasa (az index-uk) nullarol indul

//                  0       1       2
var szin_lista = [ "#0f0", "#f00", "#00f" ]

// egy lista elemere a "nev[ index ]" syntaxxal tudunk hivatkozni

var masodik_szin = szin_lista[ 1 ]

// egyebkent ez mukodik a sima objecteken is amennyiben a property nevet "" koze tesszuk
var terry_kedve = ficko[ "hangulat" ]
//                same as
//                ficko.hangulat



// bonusz
// elnevezes konvenciok (csak egyet valaszthatsz ;))

var camelCaseNaming = "minden uj szot nagy betuvel irsz"
// legkezenfekvobb es legtobbet hasznalt a librarykben

var snake_case_naming = "szavak koze underscore karaktert teszel"
// valamivel olvashatobb viszont plusz egy karakter szavankent

var lustaosszeirasamitnehezkiolvasni = "faradt vagy"
// nem ajanlatos :D

// no-such-case = Syntax Error, mert a kotojel minuszt jelent

// letezik egy console nevu object alapbol, amin a log function-t meghivva valamivel, kirja a browserben levo console-ba azt a valamit
// pl
console.log( "hello world!" )
// a consoleban a log jobb szelen irja hogy melyik script, hanyadik soran lett meghivva a console.log function
// itt amugy rakerdezhetsz valtozokra amiket definialtal, vagy meghivhatsz fuggvenyeket stb




//laterz

//comparison operators
//logical operators
//loops
//scope
//classes

//jquery
//events
//main loop
